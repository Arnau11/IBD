import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

public class Alumne {
	private static final int NUM_ASSIG = 15;
	private static final int TAM_CODI = 8;
	private static final int TAM_NOM = 20;
	private static final int TAM_ASSIG = 45;
	private static final int TAM_OBJECTE = TAM_CODI*2+TAM_NOM*2+TAM_ASSIG*2*NUM_ASSIG+4;
	
	private String codi, nom;
	private String[] assig;
	private int numAssig;
	
	public Alumne(String nom, String codi) {
		this.codi=codi;
		this.nom=nom;
		assig=new String[NUM_ASSIG];
		numAssig=0;
	}
	
	public void afegirAssig(String assignatura) {
		if (numAssig<assig.length) {
			assig[numAssig]=assignatura;
			numAssig++;
		}
	}

	public String getCodi() {
		return codi;
	}

	public void setCodi(String codi) {
		this.codi = codi;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String[] getAssig() {
		return assig;
	}

	public void setAssig(String[] assig) {
		this.assig = assig;
	}
	
	public void setAssig0() {
		this.assig[0]="INFRAESTRUCTURES PER AL BIG DATA";
	}

	public int getNumAssig() {
		return numAssig;
	}

	public void setNumAssig(int numAssig) {
		this.numAssig = numAssig;
	}

	@Override
	public String toString() {
		return "Alumne [codi=" + codi + ", nom=" + nom + ", assig=" + Arrays.toString(assig) + ", numAssig=" + numAssig
				+ "]";
	}
	
	public boolean matriculatAssignatura() {
		boolean trobat=false;
		String assignatura;
		int i=0;
		while(!trobat && i<numAssig)
		{	
			assignatura=assig[i].trim();
			if(assignatura.equalsIgnoreCase("FONAMENTS DE PROGRAMACIO")){
				trobat=true;
			}
			i++;
		}
		return trobat;
	}
	
	public static Alumne llegirDadesDeFitxer(int posicio, RandomAccessFile f) throws IOException {
		Alumne anou;
		f.seek(posicio*Alumne.TAM_OBJECTE);
		
		String aux="";
		for (int i=0; i<TAM_CODI; i++)
			aux=aux+f.readChar();
		String codi=aux;
		
		aux="";
		for (int i=0; i<TAM_NOM; i++)
			aux=aux+f.readChar();
		String nom=aux;
		anou=new Alumne(nom, codi);
		
		int numAssig=f.readInt();
		for (int j=0; j<numAssig; j++) {
			aux="";
			for (int i=0; i<TAM_ASSIG; i++)
				aux=aux+f.readChar();
			anou.afegirAssig(aux);
		}
		return(anou);
	}
	
	public void escriureDadesAFitxer(int posicio, RandomAccessFile f) throws IOException {
		f.seek(posicio*Alumne.TAM_OBJECTE);
		
		StringBuffer aux=new StringBuffer(codi);
		aux.setLength(TAM_CODI);
		f.writeChars(aux.toString());
		
		aux=new StringBuffer(nom);
		aux.setLength(TAM_NOM);
		f.writeChars(aux.toString());
		
		f.writeInt(numAssig);
		
		for (int i=0; i<numAssig; i++) {
			aux=new StringBuffer(assig[i]);
			aux.setLength(TAM_ASSIG);
			f.writeChars(aux.toString());
		}
		for (int j=numAssig; j<NUM_ASSIG; j++) {
			aux=new StringBuffer(" ");
			aux.setLength(TAM_ASSIG);
			f.writeChars(aux.toString());
		}
		
	}
	
}