
public class Index {
	private int posicio;
	private String codi;
	
	public Index(int posicio, String codi) {
		this.posicio = posicio;
		this.codi = codi;
	}

	public int getPosicio() {
		return posicio;
	}

	public String getCodi() {
		return codi;
	}
	
	
}
