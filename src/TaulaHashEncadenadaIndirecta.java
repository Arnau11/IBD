

public class TaulaHashEncadenadaIndirecta {

	private Node[] taulaElements;
	private int numElements;
	// adicional, taula per a analitzar el numero de colisions que tenim en cada posicio
	private int[] numNodesPerPosicio; 
	private int maxNumNodes;	

	public TaulaHashEncadenadaIndirecta(int capacitatTaulaHash) {
		this.taulaElements = new Node[capacitatTaulaHash];
		this.numElements = 0;
		// analitzar colisions
		numNodesPerPosicio=new int[capacitatTaulaHash];
		maxNumNodes=0;
	}

	public void afegir(String k, int pos) {
		int clauHash = k.hashCode() % taulaElements.length;
		if (clauHash<0) clauHash=clauHash*(-1);
		if (taulaElements[clauHash] == null){
			taulaElements[clauHash] = new Node(k, pos, null);
			this.numElements++;
			// analitzar colisions
			numNodesPerPosicio[clauHash]=1;
			if (numNodesPerPosicio[clauHash]>maxNumNodes) maxNumNodes=numNodesPerPosicio[clauHash];
		}
		else {
			Node nant = taulaElements[clauHash];
			Node n = nant.getSeguent();

			while (n != null && !nant.getClau().equals(k)) {
				nant = n;
				n = n.getSeguent();
			}

			if (nant.getClau().equals(k))	//substituir (element repetit, modifiquem de lloc)
				nant.setPosicio(pos); 
			else {							//inserir
				Node nouNode = new Node(k, pos, null);
				nant.setSeguent(nouNode);
				this.numElements++;
				// analitzar colisions
				numNodesPerPosicio[clauHash]++;
				if (numNodesPerPosicio[clauHash]>maxNumNodes) maxNumNodes=numNodesPerPosicio[clauHash];
			}
		}
	}

	public int esborrar(String k) {
		int clauHash = k.hashCode() % taulaElements.length;

		Node nant = taulaElements[clauHash];

		if (nant != null) {
			if (nant.getClau().equals(k)){
				taulaElements[clauHash] = nant.getSeguent();
				numElements--;
				return nant.getPosicio();
			}
			else {

				Node n = nant.getSeguent();
				while (n != null && !n.getClau().equals(k)) {
					nant = n;
					n = n.getSeguent();
				}
				
				if (n==null)
					return (-1);
				else{
					nant.setSeguent(n.getSeguent());
					numElements--;
					return n.getPosicio();
				}					
			}			
		}

		return (-1);
	}
	
	public int [] recorregut(int pos) {
		Node n = taulaElements[pos];
		int[] posicions = new int[10];
		int i=0;
		
		for(int j=0;j<posicions.length;j++) {
			posicions[j]=-1;
		}
		
		posicions[0]=-1;
		while (n != null) {
			posicions[i]=n.getPosicio();
			n = n.getSeguent();
			i++;
		}

		return posicions;
	}

	public int consultar(String k) {
		int clauHash = k.hashCode() % taulaElements.length;
		Node n = taulaElements[clauHash];

		while (n != null && !n.getClau().equals(k))
			n = n.getSeguent();

		return (n != null) ? n.getPosicio() : (-1);
	}

	public float getFactorDeCarrega() {
		return (float) numElements / taulaElements.length;
	}

	public String toString() {
		String aux="";
		for (int i = 0; i < taulaElements.length; i++) {
			aux=aux+i + ": ";

			Node nant = taulaElements[i];

			if (nant != null) {

				while (nant != null) {
					aux=aux+nant.getClau().toString() + "("
							+ nant.getPosicio() + ") ";
					nant = nant.getSeguent();
				}
			}

			aux=aux+"\n";
		}
		aux=aux+"Factor c�rrega:"+this.getFactorDeCarrega()+"\n";
		return(aux);
	}

	
	// analitzar colisions
	public String mostrarColisions() {
		String aux="";
		int[] frequencia=new int[maxNumNodes+1];
		int num, totalElements=0;
		for (int i = 0; i < taulaElements.length; i++) {
			num=numNodesPerPosicio[i];
			frequencia[num]++;
		}
		aux=aux+"Analisi de les colisions. X (quantes posicions de la taula tenen X colisions)\n";
			for (int i = 0; i <= maxNumNodes; i++) {
				totalElements=totalElements+frequencia[i]*i;
				aux=aux+i+":("+frequencia[i]+") ";
				//for(int j=0; i<frequencia[i]; j++) System.out.print("*");
				aux=aux+"\n";
			}
			aux=aux+"El numero de valors diferents que tenim a la taula es de "+totalElements+"\n";
		return(aux);
	}
		
}
