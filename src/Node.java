
public class Node {
	private String clau;
	private int posicio;
	private Node seguent;
	
	public Node(String k, int p, Node anterior) {
		clau=k;
		posicio=p;		
		seguent=anterior;
	}
	
	public String getClau() {
		return(clau);
	}
	
	public int getPosicio() {
		return(posicio);
	}
	
	public Node getSeguent() {
		return(seguent);
	}

	public void setClau(String clau) {
		this.clau = clau;
	}

	public void setPosicio(int valor) {
		this.posicio = valor;
	}

	public void setSeguent(Node seguent) {
		this.seguent = seguent;
	}
}
