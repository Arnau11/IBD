import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Main {

	static RandomAccessFile f;
	static int MAX_ALUM = 198;
	static int NUM_REP = 100;
	static LlistaIndex llista = new LlistaIndex(MAX_ALUM);
	static TaulaHashEncadenadaIndirecta taulaHash = new TaulaHashEncadenadaIndirecta(MAX_ALUM);

	public static void main(String[] args) throws IOException {
		construirTaulaIndex();
		construirTaulaDeHash();
		mostraMenu();
		Scanner teclat = new Scanner(System.in);
		int opcio = Integer.parseInt(teclat.nextLine());
		while (opcio != 5) {
			switch (opcio) {
			case 1:
				consulta1();
				consulta2();
				consulta3();
				break;
			case 2:
				modificar1();
				modificar2();
				modificar3();
				break;
			case 3:
				consultaAssignatura1();
				consultaAssignatura2();
				consultaAssignatura3();
				break;
			case 4:
				mesAssignatures1();
				mesAssignatures2();
				mesAssignatures3();
				break;
			case 5:
				break;
			}
			mostraMenu();
			opcio = Integer.parseInt(teclat.nextLine());
		}
		teclat.close();
	}

	public static void mostraMenu() {
		System.out.println("Quina opci� vols? Es duran a terme "+NUM_REP+" repeticions\n\t"
				+ "1.Calcula el temps que triga en consultar les dades d'un alumne\n\t"
				+ "2.Calcula el temps que triga en modifica les dades d'un alumne\n\t"
				+ "3.Calcula el temps que triga en consultar les dades dels alumnes que s�han matriculat de l�assignatura de FONAMENTS DE PROGRAMACI�\n\t"
				+ "4.Calcula el temps que triga en consultar les dades del alumne que s'ha matriculat de m�s assignatures\n\t"
				+ "5.Tancar Programa");
	}

	public static void consulta1() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int posicio = cercaFitxer(codi);
			Alumne aux = Alumne.llegirDadesDeFitxer(posicio, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s directe: " + (tempsf - tempsi)+" ms");
	}

	public static void consulta2() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int pos = llista.cercaDicotomica(codi);
			Alumne aux = Alumne.llegirDadesDeFitxer(pos, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula d'index: " + (tempsf - tempsi)+" ms");
	}

	public static void consulta3() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int pos = taulaHash.consultar(codi);
			Alumne aux = Alumne.llegirDadesDeFitxer(pos, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula de dispersi�: " + (tempsf - tempsi)+" ms");
	}

	public static void modificar1() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int posicio = cercaFitxer(codi);
			Alumne aux;
			aux = Alumne.llegirDadesDeFitxer(posicio, f);
			aux.setAssig0();
			aux.escriureDadesAFitxer(posicio, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s directe: " + (tempsf - tempsi)+" ms");
	}

	public static void modificar2() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int posicio = llista.cercaDicotomica(codi);
			Alumne aux;
			aux = Alumne.llegirDadesDeFitxer(posicio, f);
			aux.setAssig0();
			aux.escriureDadesAFitxer(posicio, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula d'index: " + (tempsf - tempsi)+" ms");
	}

	public static void modificar3() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int i = 0; i < NUM_REP; i++) {
			String codi = obtenirCodi();
			int posicio = taulaHash.consultar(codi);
			Alumne aux;
			aux = Alumne.llegirDadesDeFitxer(posicio, f);
			aux.setAssig0();
			aux.escriureDadesAFitxer(posicio, f);
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s taula de dispersi�: " + (tempsf - tempsi)+" ms");
	}

	public static void consultaAssignatura1() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				Alumne aux = Alumne.llegirDadesDeFitxer(i, f);
				if (aux.matriculatAssignatura()) {
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s directe: " + (tempsf - tempsi)+" ms");
	}

	public static void consultaAssignatura2() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				int posicio = llista.getPosicioDat(i);
				Alumne aux = Alumne.llegirDadesDeFitxer(posicio, f);
				if (aux.matriculatAssignatura()) {
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula d'index: " + (tempsf - tempsi)+" ms");
	}

	public static void consultaAssignatura3() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				int posicio[] = taulaHash.recorregut(i);
				for (int j = 0; j < posicio.length; j++) {
					if (posicio[j] == -1) {
						// no fem res pq esta buit
					} else {
						Alumne aux = Alumne.llegirDadesDeFitxer(posicio[j], f);
						if (aux.matriculatAssignatura()) {
						}
					}
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula de dispersi�: " + (tempsf - tempsi)+" ms");
	}

	public static void mesAssignatures1() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		int max = 0;
		Alumne mesAsignatures = null;
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				Alumne aux = Alumne.llegirDadesDeFitxer(i, f);
				if (aux.getNumAssig() > max) {
					mesAsignatures = aux;
					max = mesAsignatures.getNumAssig();
				} else if (aux.getNumAssig() == max) {
					if (aux.getNom().compareTo(mesAsignatures.getNom()) < 0) {
						mesAsignatures = aux;
						max = mesAsignatures.getNumAssig();
					}
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s directe: " + (tempsf - tempsi)+" ms");
	}

	public static void mesAssignatures2() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		int max = 0;
		Alumne mesAsignatures = null;
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				int posicio = llista.getPosicioDat(i);
				Alumne aux = Alumne.llegirDadesDeFitxer(posicio, f);
				if (aux.getNumAssig() > max) {
					mesAsignatures = aux;
					max = mesAsignatures.getNumAssig();
				} else if (aux.getNumAssig() == max) {
					if (aux.getNom().compareTo(mesAsignatures.getNom()) < 0) {
						mesAsignatures = aux;
						max = mesAsignatures.getNumAssig();
					}
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula d'index: " + (tempsf - tempsi)+" ms");
	}

	public static void mesAssignatures3() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		int max = 0;
		Alumne mesAsignatures = null;
		long tempsi, tempsf;
		tempsi = System.currentTimeMillis();
		for (int k = 0; k < NUM_REP; k++) {
			for (int i = 0; i < MAX_ALUM; i++) {
				int posicio[] = taulaHash.recorregut(i);
				for (int j = 0; j < posicio.length; j++) {
					if (posicio[j] == -1) {
						// no fem res pq esta buit
					} else {
						Alumne aux = Alumne.llegirDadesDeFitxer(posicio[j], f);
						if (aux.getNumAssig() > max) {
							mesAsignatures = aux;
							max = mesAsignatures.getNumAssig();
						} else if (aux.getNumAssig() == max) {
							if (aux.getNom().compareTo(mesAsignatures.getNom()) < 0) {
								mesAsignatures = aux;
								max = mesAsignatures.getNumAssig();
							}
						}
					}
				}
			}
		}
		tempsf = System.currentTimeMillis();
		System.out.println("Temps acc�s amb taula d'index: " + (tempsf - tempsi)+" ms");
	}

	public static String obtenirCodi() {
		int randomInt = (int) (Math.random() * MAX_ALUM) + 1;
		String codi;
		if (randomInt < 10) {
			codi = "URV_000" + randomInt;
		} else if (randomInt > 9 && randomInt < 100) {
			codi = "URV_00" + randomInt;
		} else {
			codi = "URV_0" + randomInt;
		}
		return codi;
	}

	public static void construirTaulaIndex() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		Alumne aux;
		for (int i = 0; i < MAX_ALUM; i++) {
			aux = Alumne.llegirDadesDeFitxer(i, f);
			llista.afegirAlumneIndex(i, aux.getCodi());
		}
	}

	public static void construirTaulaDeHash() throws IOException {
		f = new RandomAccessFile("MatriculaAlumnes.dat", "rw");
		Alumne aux;
		for (int i = 0; i < MAX_ALUM; i++) {
			aux = Alumne.llegirDadesDeFitxer(i, f);
			taulaHash.afegir(aux.getCodi(), i);
		}

	}

	public static int cercaFitxer(String codi) throws IOException {
		boolean trobat = false;
		int i = 0;
		Alumne aux;
		while (!trobat && i < MAX_ALUM) {
			aux = Alumne.llegirDadesDeFitxer(i, f);
			if (aux.getCodi().equals(codi))
				trobat = true;
			else
				i++;
		}
		return i;
	}

}