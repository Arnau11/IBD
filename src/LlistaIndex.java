
public class LlistaIndex {
	private int numAlumnes;
	private Index[] llista;
	
	public LlistaIndex(int num) {
		this.numAlumnes = 0;
		this.llista = new Index[num];
	}
	
	public int getPosicioDat(int posicio) {
		return(llista[posicio].getPosicio());
	}
	
	public void afegirAlumneIndex(int i, String codi)
	{
		int pos=numAlumnes-1;
		while ((pos>=0)&&(llista[pos].getCodi().compareTo(codi)>0)) {
			llista[pos+1]=llista[pos];
			pos--;
		}
		llista[pos+1]=new Index(i, codi);
		numAlumnes++;
	}
	
	public String retornaCodi(int pos) {
		System.out.println(pos);
		if(llista[pos].getCodi()==null) {
			return("-1");
		} else return(llista[pos].getCodi()+" "+llista[pos].getPosicio());
	}
	
	
	public int cercaDicotomica(String codi) {
		int l,m,h;
		boolean trobat=false;
		l=0;
		h=numAlumnes-1;
		m=-1;
		while(!trobat && l<=h) {
			m=((l+h)/2);
			//m=(h-l)/2+l;
			if(llista[m].getCodi().equals(codi)) {
				trobat=true;
			}else {
				if(llista[m].getCodi().compareTo(codi)>0) {
					h=m-1;
				}else {
					l=m+1;
				}
			}
		}
		if(trobat) {
			return(llista[m].getPosicio());
		}
		else return(-1);
	}
	
}
	

